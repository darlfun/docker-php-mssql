#!/bin/bash

for version in */ ; do
    for full in $version/*/ ; do
        subtitution=${full/\/\//-}
        tag=${subtitution::-1}
        docker build -t registry.gitlab.com/darlfun/docker-php-mssql:$tag $full
        docker push registry.gitlab.com/darlfun/docker-php-mssql:$tag
    done
done